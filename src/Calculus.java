import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

public class Calculus {
    public static void main(String[] argv) throws ExecutionException, InterruptedException {
        System.out.println("Welcome to Calculus !\nThe challenge is to solve different calculations by memory, are you up for it ?");
        System.out.println("Enter 1 if you want to try, 2 if you want to exit");
        Scanner s = new Scanner(System.in);
        int option = loopBadOption(s);
        if (option == 2 )
        {
            System.out.println("Maybe next time !");
            return;
        }
        System.out.println("Do you want to play with suggested answers ?");
        System.out.println("Enter \"yes\" if you want suggested answers to appear when solving the calculations, \"no\" if not");
        String prop = loopBadProposal(s);
        System.out.println("Difficulty ?");
        System.out.println("Enter \"simple\", \"normal\" or \"difficult\"");
        String difficulty = loopBadDifficulty(s);
        if (prop.equals("yes"))
        {
            System.out.println("Enter the number of the proposal, not the answer of the calculations.");
            loopNotUnderstood(s);
        }
        System.out.println("Are you ready ?");
        for (int i = 3; i >= 0 ; i--) {
            int finalI = i;
            CompletableFuture.runAsync(() -> {
                if (finalI > 0)
                    System.out.println(finalI);
                else
                    System.out.println("GO !\n");
            }, CompletableFuture.delayedExecutor(1, TimeUnit.SECONDS)).get();
        }
        if (prop.equals("yes"))
        {
            CalculusWithProposals(difficulty, s);
            return;
        }
        CalculusWithoutProposals(difficulty, s);
    }

    private static void loopNotUnderstood(Scanner s) {
        System.out.println("Understood ? Enter \"yes\" if you did, \"no\" if not");
        String understood = s.next();
        if (understood.equals("yes"))
            return;
        else if (understood.equals("no"))
        {
            System.out.println("You will be given 4 suggested answers, numbered from 1 to 4. If for example you think the 2nd proposal is correct, press 2");
            loopNotUnderstood(s);
        }
        else
            loopNotUnderstood(s);
    }

    private static String loopBadDifficulty(Scanner s) {
        String option = s.next();
        if (option.equals("simple") || option.equals("normal") || option.equals("difficult"))
            return option;
        else
        {
            System.out.println("That's not a valid option, please enter \"simple\", \"normal\" or \"difficult\" to select your difficulty");
            return loopBadProposal(s);
        }
    }

    private static void CalculusWithoutProposals(String difficulty, Scanner s) {
        if (difficulty.equals("simple"))
        {
            CalculusWithoutProposalsSimpleNormalDifficult(s, 21, 2);
        }
        else if (difficulty.equals("normal"))
        {
            CalculusWithoutProposalsSimpleNormalDifficult(s, 100, 2);
        }
        else
            CalculusWithoutProposalsSimpleNormalDifficult(s, 100, 5);

    }
    private static void CalculusWithProposals(String difficulty, Scanner s)
    {
        if (difficulty.equals("simple"))
            CalculusWithProposalsSimpleNormalDifficult(s, 21, 2);
        else if (difficulty.equals("normal"))
            CalculusWithProposalsSimpleNormalDifficult(s, 100, 2);
        else
            CalculusWithProposalsSimpleNormalDifficult(s, 100, 5);
    }

    private static void CalculusWithProposalsSimpleNormalDifficult(Scanner s, int maxBoundOperand, int maxBoundOperator) {
        int nbCorrect = 0;
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            int left_operand = new Random().nextInt(maxBoundOperand == 100 ? 50 : 0, maxBoundOperand);
            int right_operand = new Random().nextInt(maxBoundOperand == 100 ? 50 : 0, maxBoundOperand);
            int operatorNumber = new Random().nextInt(maxBoundOperator);
            switch (operatorNumber) {
                case 0 -> nbCorrect += CalculPlusWithProposals(s, left_operand, right_operand);
                case 1 -> nbCorrect += CalculMinusWithProposals(s, left_operand, right_operand);
                case 2 -> nbCorrect += CalculMulWithProposals(s, left_operand, right_operand);
                case 3 -> nbCorrect += CalculDivWithProposals(s, left_operand, right_operand);
            }
        }
        System.out.println("Number of good answers: " + nbCorrect);
        System.out.println("You spent " + ((System.currentTimeMillis() - startTime) / 1000) + " seconds to complete those calculus !");
        System.out.println("Thank you for playing, see you next time !");
    }

    private static int[] ProposalArray(int result)
    {
        int[] proposals = new int[4];
        boolean goodAnswer = false;
        for (int i = 0; i < 4; i++) {
            if ((new Random().nextInt(4) == 0 || i == 3) && !goodAnswer)
            {
                goodAnswer = true;
                proposals[i] = result;
            }
            else
            {
                int a = new Random().nextInt(1,20);
                for (int j = 0; j < i; j++) {
                    if (result + a == proposals[j] || Math.abs(result - a) == proposals[j])
                    {
                        a = new Random().nextInt(1,20);
                        j = -1;
                    }
                }
                if (new Random().nextInt(2) == 0)
                    proposals[i] = result + a;
                else
                    proposals[i] = Math.abs(result - a);
            }
            System.out.println((i + 1) + ") " + proposals[i]);
        }
        return proposals;
    }

    private static int CalculDivWithProposals(Scanner s, int left_operand, int right_operand) {
        left_operand = 300 - left_operand;
        right_operand = right_operand % 20;
        while (left_operand % right_operand != 0)
            right_operand++;
        System.out.println(left_operand + " / " + right_operand + " = ?");

        int[] proposals = ProposalArray(left_operand / right_operand);
        int result = loopBadResult(s);
        if (result <= 4 && result >= 1 && proposals[result - 1] == left_operand / right_operand)
        {
            System.out.println("You are correct !");
            return 1;
        }
        System.out.println("You are Wrong !");
        return 0;
    }

    private static int CalculMulWithProposals(Scanner s, int left_operand, int right_operand) {
        right_operand = right_operand % 30;
        System.out.println(left_operand + " * " + right_operand + " = ?");
        int[] proposals = ProposalArray(left_operand * right_operand);
        int result = loopBadResult(s);
        if (result <= 4 && result >= 1 && proposals[result - 1] == left_operand * right_operand)
        {
            System.out.println("You are correct !");
            return 1;
        }
        System.out.println("You are Wrong !");
        return 0;
    }

    private static int CalculMinusWithProposals(Scanner s, int left_operand, int right_operand) {
        if (left_operand < right_operand)
        {
            int tmp = left_operand;
            left_operand = right_operand;
            right_operand = tmp;
        }
        System.out.println(left_operand + " - " + right_operand + " = ?");
        int[] proposals = ProposalArray(left_operand - right_operand);
        int result = loopBadResult(s);
        if (result <= 4 && result >= 1 && proposals[result - 1] == left_operand - right_operand)
        {
            System.out.println("You are correct");
            return 1;
        }
        System.out.println("You are wrong !");
        return 0;
    }

    private static int CalculPlusWithProposals(Scanner s, int left_operand, int right_operand) {
        System.out.println(left_operand + " + " + right_operand + " = ?");
        int[] proposals = ProposalArray(left_operand + right_operand);
        int result = loopBadResult(s);
        if (result <= 4 && result >= 1 && proposals[result - 1] == left_operand + right_operand)
        {
            System.out.println("You are correct !");
            return 1;
        }
        System.out.println("You are Wrong !");
        return 0;
    }

    private static void CalculusWithoutProposalsSimpleNormalDifficult(Scanner s, int maxBoundOperand, int maxBoundOperator) {
        int nbCorrect = 0;
        long startTime = System.currentTimeMillis();
        for (int i = 0; i < 10; i++) {
            int left_operand = new Random().nextInt(maxBoundOperand == 100 ? 50 : 0, maxBoundOperand);
            int right_operand = new Random().nextInt(maxBoundOperand == 100 ? 50 : 0, maxBoundOperand);
            int operatorNumber = new Random().nextInt(maxBoundOperator);
            switch (operatorNumber) {
                case 0 -> nbCorrect += CalculPlusWithoutProposals(s, left_operand, right_operand);
                case 1 -> nbCorrect += CalculMinusWithoutProposals(s, left_operand, right_operand);
                case 2 -> nbCorrect += CalculMulWithoutProposals(s, left_operand, right_operand);
                case 3 -> nbCorrect += CalculDivWithoutProposals(s, left_operand, right_operand);
            }
        }
        System.out.println("Number of good answers: " + nbCorrect);
        System.out.println("You spent " + ((System.currentTimeMillis() - startTime) / 1000) + " seconds to complete those calculus !");
        System.out.println("Thank you for playing, see you next time !");
    }

    private static int CalculDivWithoutProposals(Scanner s, int left_operand, int right_operand) {
        left_operand = 300 - left_operand;
        right_operand = right_operand % 20;
        while (left_operand % right_operand != 0)
            right_operand++;
        System.out.println(left_operand + " / " + right_operand + " = ?");
        int result = loopBadResult(s);
        if (result == left_operand / right_operand)
        {
            System.out.println("You are correct !");
            return 1;
        }
        System.out.println("You are Wrong !");
        return 0;
    }

    private static int CalculMinusWithoutProposals(Scanner s, int left_operand, int right_operand) {
        if (left_operand < right_operand)
        {
            int tmp = left_operand;
            left_operand = right_operand;
            right_operand = tmp;
        }
        System.out.println(left_operand + " - " + right_operand + " = ?");
        int result = loopBadResult(s);
        if (result == left_operand - right_operand)
        {
            System.out.println("You are correct");
            return 1;
        }
        System.out.println("You are wrong !");
        return 0;
    }

    private static int CalculPlusWithoutProposals(Scanner s, int left_operand, int right_operand) {
        System.out.println(left_operand + " + " + right_operand + " = ?");
        int result = loopBadResult(s);
        if (result == left_operand + right_operand)
        {
            System.out.println("You are correct !");
            return 1;
        }
        System.out.println("You are Wrong !");
        return 0;
    }
    private static int CalculMulWithoutProposals(Scanner s, int left_operand, int right_operand) {
        right_operand = right_operand % 30;
        System.out.println(left_operand + " * " + right_operand + " = ?");
        int result = loopBadResult(s);
        if (result == left_operand * right_operand)
        {
            System.out.println("You are correct !");
            return 1;
        }
        System.out.println("You are Wrong !");
        return 0;
    }

    private static int loopBadResult(Scanner s) {
        try {
            return s.nextInt();
        } catch (Exception e)
        {
            System.out.println("That's not a valid option, enter a number");
            s.nextLine();
            return loopBadResult(s);
        }
    }
    private static int loopBadOption(Scanner s)
    {
        try {
            int option = s.nextInt();
            if (option != 1 && option != 2)
            {
                System.out.println("That's not a valid option, please enter 1 if you want to try, 2 if you want to exit");
                return loopBadOption(s);
            }
            return option;
        } catch (Exception e)
        {
            System.out.println("That's not a valid option, enter press 1 if you want to try, 2 if you want to exit");
            s.nextLine();
            return loopBadOption(s);
        }
    }

    private static String loopBadProposal(Scanner s)
    {
        String option = s.next();
        if (option.equals("yes") || option.equals("no"))
        {
            return option;
        }
        else
        {
            System.out.println("That's not a valid option, please enter \"yes\" if you want proposals, \"no\" if you don't");
            return loopBadProposal(s);
        }
    }
}
