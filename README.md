How to play :

You will need Java installed on your computer, with jdk-19 minimum. 

The compiled code will be given to you, it's called "Calculus.class" Or just "Calculus". 
And the code will be also given, named "Calculus.java"

If you want to launch the game, open a terminal and in the src file launch :

java Calculus.class 

it will normally start the game in the terminal you launched the program from. 
Then let it guide you and Enjoy. 

I did a few modes :
- You can choose the difficulty (simple, normal or difficult)
- You can play with or without suggested answers

The game is to solve as many calculations as possible among the ten.